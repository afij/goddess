
#include "DetectorConstruction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"

#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4VisAttributes.hh"
#include "G4Box.hh"

#include "ORRUBA.hh"
#include "Gammasphere.hh"
#include "MaterialsManager.hh"

DetectorConstruction::DetectorConstruction()
  :worldSolid(0), worldLogical(0), worldPhysical(0) 
{
  
    DefineMaterials();
    worldSize = 2.0*m;

}

DetectorConstruction::~DetectorConstruction()
{
  
}

void DetectorConstruction::DefineMaterials()
{
    MaterialsManager* materialsManager = MaterialsManager::GetInstance();
    worldMat = materialsManager->GetAir();
}


G4VPhysicalVolume* DetectorConstruction::Construct()
{

    // define world
    worldSolid = new G4Box("world", worldSize, worldSize, worldSize);
    worldLogical = new G4LogicalVolume(worldSolid, worldMat,"world");
    worldPhysical = new G4PVPlacement(0, G4ThreeVector(), worldLogical, 
                                     "world", 0, false, 0, false);

                      
    ORRUBA* orruba = new ORRUBA();
    orruba->Construct(worldLogical);
    orruba->ConstructSDandField();
    
    
                        
    Gammasphere* gammasphere = new Gammasphere();
    gammasphere->Construct(worldLogical); 
    gammasphere->ConstructSDandField();                   
    return worldPhysical;
}

