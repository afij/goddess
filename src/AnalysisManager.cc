
// $Id: AnalysisManager.cc 12.16.2016 A. Fijalkowska $
//
/// \file AnalysisManager.cc
/// \brief Definition of the AnalysisManager singleton class
//
//
#include "AnalysisManager.hh"
#include "G4SystemOfUnits.hh"
AnalysisManager::AnalysisManager()
{
   rootManager = G4RootAnalysisManager::Instance();	
   rootManager->SetVerboseLevel(1);
   rootManager->SetFirstHistoId(0);
   rootManager->SetFirstNtupleId(0);
   rootManager->SetFirstNtupleColumnId(0);  
   nrOfCreatedTuple = 0;

}

void AnalysisManager::CreateOutput(G4String filename)
{
   rootManager->OpenFile(filename);
   CreateTuple();   
}


void AnalysisManager::SaveOutput()
{
   rootManager->Write();
   rootManager->CloseFile();	
}

void AnalysisManager::CreateTuple()
{
   CreateOrrubaTuple();
   CreateGammasphereTuple();
   CreateNrOfCountsTuple();
}


void AnalysisManager::CreateOrrubaTuple()
{
   std::cout << "create orruba tuple" << std::endl;
   rootManager->CreateNtuple("OrrubaInfo", "Energy dep in ORRUBA");
   rootManager->CreateNtupleIColumn("eventID");
   rootManager->CreateNtupleIColumn("detID");
   rootManager->CreateNtupleDColumn("enDep");
   rootManager->FinishNtuple();
   orrubaTupleId = nrOfCreatedTuple++;
   
   int colId = 0;
   rootManager->FillNtupleIColumn(orrubaTupleId, colId, 0); 
   rootManager->FillNtupleIColumn(orrubaTupleId, ++colId, 0);
   rootManager->FillNtupleDColumn(orrubaTupleId, ++colId, 0.);		   
   rootManager->AddNtupleRow(orrubaTupleId);   
   
}


void AnalysisManager::CreateGammasphereTuple()
{
   std::cout << "create gammasphere tuple" << std::endl;
   rootManager->CreateNtuple("GammasphereInfo", "Energy dep in Gammasphere");
   rootManager->CreateNtupleIColumn("eventID");
   rootManager->CreateNtupleIColumn("detID");
   rootManager->CreateNtupleDColumn("enDep");
   rootManager->FinishNtuple();
   gammasphereTupleId = nrOfCreatedTuple++;
   
   int colId = 0;
   rootManager->FillNtupleIColumn(gammasphereTupleId, colId, 0); 
   rootManager->FillNtupleIColumn(gammasphereTupleId, ++colId, 0);
   rootManager->FillNtupleDColumn(gammasphereTupleId, ++colId, 0.);	   
   rootManager->AddNtupleRow(gammasphereTupleId);   
   
}

void AnalysisManager::CreateNrOfCountsTuple()
{
    std::cout << "create nr of counts tuple" << std::endl;
    rootManager->CreateNtuple("NrOfCountsInfo", "Tot nr of counts");
    rootManager->CreateNtupleIColumn("nrOfCounts");
    rootManager->FinishNtuple();
    nrOrCountsTupleId = nrOfCreatedTuple++;
    
    int colId = 0;
    rootManager->FillNtupleIColumn(nrOrCountsTupleId, colId, 0); 	   
    rootManager->AddNtupleRow(nrOrCountsTupleId);  
}


void AnalysisManager::AddOrrubaHit(DetectorHitsCollection* orrubaHC, G4int eventId)
{
    G4int modules = orrubaHC->entries();
    for(G4int i=0; i != modules; i++)
    {
	   G4int moduleIndex = (*orrubaHC)[i]->GetModuleIndex();
	   //G4int nrOfInteractions = (*orrubaHC)[i]->GetNrOfInteractions();
       G4double energyDep = (*orrubaHC)[i]->GetEnergyDeposit();

	   int colId = 0;
	   rootManager->FillNtupleIColumn(orrubaTupleId, colId, eventId); 
	   rootManager->FillNtupleIColumn(orrubaTupleId, ++colId, moduleIndex);
		rootManager->FillNtupleDColumn(orrubaTupleId, ++colId, energyDep/keV);		   
		rootManager->AddNtupleRow(orrubaTupleId);

    }	
}



void AnalysisManager::AddGammasphereHit(DetectorHitsCollection* gammasphereHC, G4int eventId)
{
    G4int modules = gammasphereHC->entries();
    for(G4int i=0; i != modules; i++)
    {
	   G4int moduleIndex = (*gammasphereHC)[i]->GetModuleIndex();
	   //G4int nrOfInteractions = (*gammasphereHC)[i]->GetNrOfInteractions();

	   G4double energyDep = (*gammasphereHC)[i]->GetEnergyDeposit();
	   int colId = 0;
	   rootManager->FillNtupleIColumn(gammasphereTupleId, colId, eventId); 
	   rootManager->FillNtupleIColumn(gammasphereTupleId, ++colId, moduleIndex);
	   rootManager->FillNtupleDColumn(gammasphereTupleId, ++colId, energyDep/keV);		   
	   rootManager->AddNtupleRow(gammasphereTupleId);
    }	
}


void AnalysisManager::AddNrOfEvent(G4int nrOfEvents)
{
    int colId = 0;
    rootManager->FillNtupleIColumn(nrOrCountsTupleId, colId, nrOfEvents); 	
    rootManager->AddNtupleRow(nrOrCountsTupleId);
}
	
AnalysisManager *AnalysisManager::s_instance = 0;
