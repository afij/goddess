#include "RunAction.hh"
#include "G4Run.hh"
#include "G4String.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"
#include "G4ios.hh"
#include "RunActionMessenger.hh"

#include "AnalysisManager.hh"

extern std::ofstream outFile;

RunAction::RunAction(): G4UserRunAction()
{
    AnalysisManager* analysisManager = AnalysisManager::GetInstance(); 
    totNrOfEvents = 0;
    analysisManager->CreateOutput("./GODDESS.root");
    runMessenger = new RunActionMessenger(this);
}

RunAction::~RunAction()
{
   AnalysisManager* analysisManager = AnalysisManager::GetInstance();
   analysisManager->AddNrOfEvent(totNrOfEvents);
   analysisManager->SaveOutput();
   delete runMessenger;
}

void RunAction::BeginOfRunAction(const G4Run* aRun)
{
   G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl; 

}

void RunAction::EndOfRunAction(const G4Run* aRun)
{
  G4cout << "Closed output file " << G4endl;
  totNrOfEvents += aRun->GetNumberOfEvent();
}

