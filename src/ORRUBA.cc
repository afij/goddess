// $Id: ORRUBA.cc 03.02.2016 A Fijalkowska $
//
/// \file ORRUBA.cc
/// \brief Implementation of the ORRUBA, code pieces taken from Travis Baugher code
//
//
#include "ORRUBA.hh"

// CADMESH //
#include "CADMesh.hh"

// GEANT4 //
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"

#include "G4Box.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4SDManager.hh"
#include "MaterialsManager.hh"

ORRUBA::ORRUBA(): sil_E_logical(0), sil_dE_logical(0), orrubaSD(0)
{
  n_sil_per_ring = 12;

  sil_z_offset = 40.843*mm;
  sil_width = 40.*mm;
  sil_length = 75.0*mm;
  sil_E_thick = 1.0*mm;
  sil_dE_thick = 0.065*mm;
  sil_E_radius = 101.10*mm;
  sil_dE_radius = 96.17*mm;
  
  //mainZOffset = -38.1*mm;
  mainZOffset = 0.*mm;
  DefineMaterials();
  DefineColors();
}

ORRUBA::~ORRUBA()
{
}


void ORRUBA::DefineMaterials()
{ 
    MaterialsManager* materialsManager = MaterialsManager::GetInstance();

    detector_mounts_mat = materialsManager->GetG10();
    ORRUBA_frame_mat = materialsManager->GetAluminum();
    sil_mat = materialsManager->GetSilicon();
}


void ORRUBA::DefineColors()
{ 
  yellow = new G4VisAttributes(G4Color(1.0, 1.0, 0.0));
  yellow->SetForceSolid(true);
  gray = new G4VisAttributes(G4Color(0.5, 0.5, 0.5));
  gray->SetForceSolid(true);
  orange = new G4VisAttributes(G4Color(1.0, 0.647, 0.0));
  orange->SetForceSolid(true);
}

void ORRUBA::Construct(G4LogicalVolume* world_logical)
{
    ConstructSensitivePart(world_logical);
    ConstructNonSensitivePart(world_logical);

}
void ORRUBA::ConstructSensitivePart(G4LogicalVolume* world_logical)
{     
  // +y-axis is up, +z-axis is beam direction, +x axis is beam-left
  G4int i;
  G4int det_num = 0;// continuous detector numbering index
  G4ThreeVector position(0.0, sil_E_radius, -sil_z_offset + mainZOffset);
  G4RotationMatrix rotation = G4RotationMatrix();
  rotation.rotateZ(0.*deg);
  G4Transform3D transform = G4Transform3D(rotation, position); 
  G4double phi = 2*3.14159/n_sil_per_ring;


  // create silicon 1000 um E detectors
  G4VSolid* sil_E_solid = new G4Box("sil_E_solid", sil_width/2, sil_E_thick/2, sil_length/2);
  sil_E_logical = new G4LogicalVolume(sil_E_solid, sil_mat, "sil_E_logical");

  // create dE detectors
  G4VSolid* sil_dE_solid = new G4Box("sil_dE", sil_width/2, sil_dE_thick/2, sil_length/2);
  sil_dE_logical = new G4LogicalVolume(sil_dE_solid, sil_mat, "sil_dE");

  // backward E detectors (det_num=0-11 clockwise from top (y-axis))
  //det_num = 200;
  for (i = 0; i < n_sil_per_ring; i++) {
    new G4PVPlacement(transform, sil_E_logical, "sil_E", world_logical, false, det_num, false);
    rotation.rotateZ(phi);
    position.rotateZ(phi);
    transform = G4Transform3D(rotation, position);
    det_num++;
  }

  // forward E detectors (det_num=12-23 clockwise from top (y-axis))
  position.setZ(sil_z_offset + mainZOffset);
  transform = G4Transform3D(rotation, position);
  for (i = 0; i < n_sil_per_ring; i++) {
    new G4PVPlacement(transform, sil_E_logical, "sil_E", world_logical, false, det_num, false);
    rotation.rotateZ(phi);
    position.rotateZ(phi);
    transform = G4Transform3D(rotation, position);
    det_num++;
  }

  det_num = 100;
  // backward dE detectors (det_num 100-111 clockwise from top (y-axis)
  position.setY(sil_dE_radius);
  position.setZ(-sil_z_offset + mainZOffset);
  transform = G4Transform3D(rotation, position);
  for (i = 0; i < n_sil_per_ring; i++) {
    new G4PVPlacement(transform, sil_dE_logical, "sil_dE", world_logical, false, det_num, false);
    rotation.rotateZ(phi);
    position.rotateZ(phi);
    transform = G4Transform3D(rotation, position);
    det_num++;
  }

  // forward dE detectors (det_num 112-123 clockwise from top (y-axis)
  position.setZ(sil_z_offset + mainZOffset);
  transform = G4Transform3D(rotation, position);
  for (i = 0; i < n_sil_per_ring; i++) {
    new G4PVPlacement(transform, sil_dE_logical, "sil_dE", world_logical, false, det_num, false);
    rotation.rotateZ(phi);
    position.rotateZ(phi);
    transform = G4Transform3D(rotation, position);
    det_num++;
  }

  sil_dE_logical-> SetVisAttributes(yellow);
  sil_E_logical->SetVisAttributes(orange);
}  
  
void ORRUBA::ConstructNonSensitivePart(G4LogicalVolume* world_logical)
{

  G4double stlMaxX= 215.53466;
  G4double stlMaxY= 215.53466;
  G4double stlMaxZ= 213.36;
  G4double stlMinX = 0.;
  G4double stlMinY = 0.;
  G4double stlMinZ = 0.;
  G4ThreeVector offset;

  // detector mounts   
  offset = G4ThreeVector(-((stlMaxX-stlMinX)/2.0+stlMinX), 
                         -((stlMaxY-stlMinY)/2.0+stlMinY), 
                         -((stlMaxZ-stlMinZ)/2.0+stlMinZ) + mainZOffset);
  CADMesh *detector_mounts = new CADMesh("models/detector_mounts.stl", "STL", mm, offset, false);
  G4VSolid* detector_mounts_solid = detector_mounts->TessellatedMesh();
  detector_mounts_logical = new G4LogicalVolume(detector_mounts_solid, detector_mounts_mat, "detector_mounts", 0, 0, 0);
  new G4PVPlacement(0, G4ThreeVector(), detector_mounts_logical, "detector_mounts", world_logical, false, 0, false);

  // ORRUBA frame

  stlMaxX= 245.80749;
  stlMaxY= 245.8072;
  stlMaxZ= 228.6;
  offset = G4ThreeVector(-((stlMaxX-stlMinX)/2.0+stlMinX), 
                         -((stlMaxY-stlMinY)/2.0+stlMinY), 
                         -((stlMaxZ-stlMinZ)/2.0+stlMinZ) + mainZOffset);
  CADMesh* ORRUBA_frame = new CADMesh("models/ORRUBA_frame.stl", "STL", mm, offset, false);
  G4VSolid* ORRUBA_frame_solid = ORRUBA_frame->TessellatedMesh();
  ORRUBA_frame_logical = new G4LogicalVolume(ORRUBA_frame_solid, ORRUBA_frame_mat, "ORRUBA_frame", 0, 0, 0);
  new G4PVPlacement(0, G4ThreeVector(), ORRUBA_frame_logical, "ORRUBA_frame", world_logical, false, 0, false);

  ORRUBA_frame_logical->SetVisAttributes(gray);
}


void ORRUBA::ConstructSDandField()
{
     if (!orrubaSD) 
     {
        G4cout << "Construction /orrubaSD sensitive detector" << G4endl;
        orrubaSD = new DetectorSD("/orrubaSD", "OrrubaHitsCollection");
        orrubaSD->SetModuleDeph(0);
    }

     G4SDManager* SDman = G4SDManager::GetSDMpointer();
     SDman->AddNewDetector(orrubaSD);
     sil_dE_logical->SetSensitiveDetector(orrubaSD);	
	 sil_E_logical->SetSensitiveDetector(orrubaSD);
}

//DetectorSD* ORRUBA::orrubaSD = 0;
