
#include "Gammasphere.hh"
#include "CADMesh.hh"

// GEANT4 //
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4UnionSolid.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4Material.hh"
#include "MaterialsManager.hh"
#include "G4VisAttributes.hh"
#include "G4SDManager.hh"

Gammasphere::Gammasphere()
   :ge_crystal_logical(0), BGOTypeB_alu_logical(0),
   BGOTypeB_logical(0), BGOTypeC_logical(0), 
   BGOTypeD_logical(0), carbon_disk_logical(0), backplug_logical(0),gammasphereSD(0)
{
  
    DefineMaterials();
    DefineColors();

    ge_outer_diameter = 72.0*mm;
    ge_inner_diameter = 0.0*mm;
    ge_length = 76.6*mm;
    ge_taper_length = 20.00*mm;
    ge_taper_diameter = (ge_outer_diameter-ge_taper_length*tan(7.5*3.14159/180.0))*mm;

    carbon_disk_diameter = ge_outer_diameter;
    carbon_disk_thickness = 5.0*mm;
	
	backplug_diameter = ge_outer_diameter;
	backplug_thickness = 40.0*mm;

    heavimet_thickness = 30.5562*mm; // 30.5563 mm is the Z offset from 0,0,0 in the drawings

    absorber1_thickness = 0.127*mm;
    absorber2_thickness = 0.127*mm;

    GS_ge_radius = 250.0*mm;
    GS_BGO_radius = 215.0*mm;
    GSfilename = "GSAngles.txt";
	DefineMaterials();
	DefineColors();
  
}

Gammasphere::~Gammasphere()
{
}
void Gammasphere::DefineMaterials()
{
    MaterialsManager* matManager = MaterialsManager::GetInstance();
    flowerpot_mat = matManager->GetAluminum();
    gammasphere_shell_mat = matManager->GetAluminum();
    ge_mat = matManager->GetGermanium();
    BGO_mat = matManager->GetBGO();
    chamber_mat = matManager->GetAluminum();
	alu_mat = matManager->GetAluminum();

    HM_mat = matManager->GetHeavyMetal();
    absorber1_mat = matManager->GetCopper();
    absorber2_mat = matManager->GetTantalum();

    flange_mat = matManager->GetStainSteel();
    carbon_disk_mat = matManager->GetCopper();
    detector_mounts_mat = matManager->GetG10();
    endcaps_mat = matManager->GetAluminum();
    slewing_ring_mat = matManager->GetAluminum();  
}

void Gammasphere::DefineColors()
{ 
    yellow = new G4VisAttributes(G4Color(1.0, 1.0, 0.0));
    gray = new G4VisAttributes(G4Color(0.5, 0.5, 0.5));
    white = new G4VisAttributes(G4Color(1.0, 1.0, 1.0));
    red = new G4VisAttributes(G4Color(1.0, 0.0, 0.0));
    green = new G4VisAttributes(G4Color(0.0, 1.0, 0.0));
    blue = new G4VisAttributes(G4Color(0.0, 0.0, 1.0));
    orange = new G4VisAttributes(G4Color(1.0, 0.647, 0.0));
    invisible = new G4VisAttributes();
}


void Gammasphere::Construct(G4LogicalVolume* world)
{
    ConstructGermanium();
    ConstructBGO();
    ConstructHMCollimator();
    ConstructAndLocateAdditionalElements(world);
    LocateElements(world);
}


void Gammasphere::ConstructBGO()
{
    G4double stlMaxX;
    G4double stlMaxY;
    G4double stlMaxZ;
    G4ThreeVector offset;
  
    // BGO type "B"
    stlMaxX= 89.77;
    stlMaxY= 76.24;
    stlMaxZ= 245.9482;
    G4double BGO_length = stlMaxZ-heavimet_thickness;
    offset = G4ThreeVector(-stlMaxX, -stlMaxY, -heavimet_thickness);
    CADMesh *CADTypeB_alu = new CADMesh("models/BGO_det_B_alu.stl", "STL", mm, offset, false);
    G4VSolid* BGOTypeB_alu_solid = CADTypeB_alu->TessellatedMesh();
    BGOTypeB_alu_logical = new G4LogicalVolume(BGOTypeB_alu_solid, alu_mat, "BGOtypeB_alu", 0, 0, 0);

    stlMaxX= 88.58;
    stlMaxY= 75.22;
    stlMaxZ= 244.51;
    BGO_length = stlMaxZ-heavimet_thickness;
    offset = G4ThreeVector(-stlMaxX, -stlMaxY, -heavimet_thickness-0.02);
    CADMesh *CADTypeB = new CADMesh("models/BGO_det_B_scaled.stl", "STL", mm, offset, false);
    G4VSolid* BGOTypeB_solid = CADTypeB->TessellatedMesh();
    BGOTypeB_logical = new G4LogicalVolume(BGOTypeB_solid, BGO_mat, "BGOtypeB", 0, 0, 0);

    // BGO type "C"
    stlMaxX= 89.80;
    stlMaxY= 76.55;
    stlMaxZ= 245.9482;
    offset = G4ThreeVector(-stlMaxX, -stlMaxY, -heavimet_thickness);
    CADMesh *CADTypeC = new CADMesh("models/BGO_det_C.stl", "STL", mm, offset, false);
    G4VSolid* BGOTypeC_solid = CADTypeC->TessellatedMesh();
    BGOTypeC_logical = new G4LogicalVolume(BGOTypeC_solid, BGO_mat, "BGOtypeC", 0, 0, 0);
  
    // BGO type "D"
    stlMaxX= 89.82;
    stlMaxY= 79.35;
    stlMaxZ= 245.9482;
    offset = G4ThreeVector(-stlMaxX, -stlMaxY, -heavimet_thickness);
    CADMesh *CADTypeD = new CADMesh("models/BGO_det_D.stl", "STL", mm, offset, false);
    G4VSolid* BGOTypeD_solid = CADTypeD->TessellatedMesh();
    BGOTypeD_logical = new G4LogicalVolume(BGOTypeD_solid, BGO_mat, "BGOtypeD", 0, 0, 0);
  
  	// BGO backplugs
  	G4double stlMinX= 0.10563216;
    G4double stlMinY= 0.026418482;
    G4double stlMinZ= 1e-06;
	stlMaxX= 72.000001;
	stlMaxY= 71.973584;
	stlMaxZ= 40.000001;
	offset = G4ThreeVector(-((stlMaxX-stlMinX)/2.0+stlMinX), 
	                       -((stlMaxY-stlMinY)/2.0+stlMinY), 
	                       -((stlMaxZ-stlMinZ)/2.0+stlMinZ));
 	CADMesh *CAD_backplug = new CADMesh("models/backplug.stl","STL", mm, offset, false);
	G4VSolid* backplug_solid = CAD_backplug->TessellatedMesh();
	backplug_logical = new G4LogicalVolume(backplug_solid, BGO_mat, "backplug", 0, 0, 0);
	
	
	
    BGOTypeB_alu_logical->SetVisAttributes(green);
    BGOTypeB_logical->SetVisAttributes(red);
    BGOTypeC_logical->SetVisAttributes(orange);
    BGOTypeD_logical->SetVisAttributes(blue);
    backplug_logical->SetVisAttributes(invisible);
}

void Gammasphere::ConstructHMCollimator()
{
    G4double stlMaxX;
    G4double stlMaxY;
    G4double stlMaxZ;
    G4ThreeVector offset;

    // B
    stlMaxX = 44.82;
    stlMaxY = 38.06; 
    stlMaxZ = 0.0;
    offset = G4ThreeVector(-stlMaxX, -stlMaxY, -stlMaxZ);
    CADMesh *CADHM_B = new CADMesh("models/HM_det_B.stl", "STL", mm, offset, false);
    G4VSolid* HM_B_solid = CADHM_B->TessellatedMesh();
    HM_B_logical = new G4LogicalVolume(HM_B_solid, HM_mat, "HM_B", 0, 0, 0);

    // C
    stlMaxX = 44.82; 
    stlMaxY = 38.06; 
    stlMaxZ = 0.0;
    offset = G4ThreeVector(-stlMaxX, -stlMaxY, -stlMaxZ);
    CADMesh *CADHM_C = new CADMesh("models/HM_det_C.stl", "STL", mm, offset, false);
    G4VSolid* HM_C_solid = CADHM_C->TessellatedMesh();
    HM_C_logical = new G4LogicalVolume(HM_C_solid, HM_mat, "HM_C", 0, 0, 0);

    // D
    stlMaxX = 44.82; 
    stlMaxY = 38.06; 
    stlMaxZ = 0.0;
    offset = G4ThreeVector(-stlMaxX, -stlMaxY, -stlMaxZ);
    CADMesh *CADHM_D = new CADMesh("models/HM_det_D.stl", "STL", mm, offset, false);
    G4VSolid* HM_D_solid = CADHM_D->TessellatedMesh();
    HM_D_logical = new G4LogicalVolume(HM_D_solid, HM_mat, "HM_D", 0, 0, 0);
  
    HM_B_logical->SetVisAttributes(invisible);
    HM_C_logical->SetVisAttributes(invisible);
    HM_D_logical->SetVisAttributes(invisible);  
  
  
  
}

void Gammasphere::ConstructGermanium()
{

    G4Tubs *ge_cyl = new G4Tubs("ge_cyl", ge_inner_diameter/2.0, ge_outer_diameter/2.0, (ge_length-ge_taper_length)/2.0, 0.0*deg, 360.0*deg);
    G4Cons *ge_taper = new G4Cons("ge_taper", 0., ge_taper_diameter/2.0, 0.0, ge_outer_diameter/2.0, ge_taper_length/2.0, 0.0, 360.0*degree);
    G4ThreeVector position = G4ThreeVector(0.0, 0.0,-ge_length/2);
    G4RotationMatrix rotation = G4RotationMatrix();
    G4Transform3D transform = G4Transform3D(rotation, position);
    G4VSolid* ge_crystal_solid = new G4UnionSolid("GSCrystal", ge_cyl, ge_taper, transform);
    ge_crystal_logical = new G4LogicalVolume(ge_crystal_solid, ge_mat, "GSCrystal");

	G4VSolid* carbon_disk_solid = new G4Tubs("carbon_disk", 0, carbon_disk_diameter/2.0, carbon_disk_thickness/2.0, 0.0*deg, 360.0*deg);
	carbon_disk_logical = new G4LogicalVolume(carbon_disk_solid, carbon_disk_mat, "carbon_disk");


    // absorbers
    G4VSolid* absorber1_solid = new G4Tubs("absorber1", 0.0, ge_taper_diameter/2.0, absorber1_thickness/2.0, 0.0*degree, 360.0*degree);
    G4VSolid* absorber2_solid = new G4Tubs("absorber2", 0.0, ge_taper_diameter/2.0, absorber2_thickness/2.0, 0.0*degree, 360.0*degree);
    absorber1_logical = new G4LogicalVolume(absorber1_solid, absorber1_mat, "absorber1", 0, 0, 0);
    absorber2_logical = new G4LogicalVolume(absorber2_solid, absorber2_mat, "absorber2", 0, 0, 0);

    ge_crystal_logical->SetVisAttributes(invisible);
    absorber1_logical->SetVisAttributes(invisible);
    absorber2_logical->SetVisAttributes(invisible);
	carbon_disk_logical->SetVisAttributes(invisible);
}


void Gammasphere::ConstructAndLocateAdditionalElements(G4LogicalVolume *world)
{
    G4double stlMaxX;
    G4double stlMaxY;
    G4double stlMaxZ;
    G4double stlMinX;
    G4double stlMinY;
    G4double stlMinZ;
    G4ThreeVector offset;
    
    // scattering chamber
    stlMinX= 0.60727814;
    stlMinY= 1.2124811;
    stlMinZ= 9.9999999e-07 ;
    stlMaxX= 354.99272;
    stlMaxY= 354.38752;
    stlMaxZ= 346.89204;
    offset = G4ThreeVector(-((stlMaxX-stlMinX)/2.0+stlMinX), 
                           -((stlMaxY-stlMinY)/2.0+stlMinY), 
                           -((stlMaxZ-stlMinZ)/2.0+stlMinZ));
    CADMesh* scattering_chamber = new CADMesh("models/scattering_chamber.stl", "STL", mm, offset, false);
    G4VSolid* scattering_chamber_solid = scattering_chamber->TessellatedMesh();
    scattering_chamber_logical = new G4LogicalVolume(scattering_chamber_solid, chamber_mat, "scattering_chamber", 0, 0, 0);
    new G4PVPlacement(0, G4ThreeVector(), scattering_chamber_logical, "scattering_chamber", world, false, 0, false);

    // flowerpot
    stlMinX= 1e-06; 
    stlMinY= 1e-06;
    stlMinZ= 228.6 ;
    stlMaxX= 431.8;
    stlMaxY= 431.8;
    stlMaxZ= 568.325;
    offset = G4ThreeVector(-((stlMaxX-stlMinX)/2.0+stlMinX), -((stlMaxY-stlMinY)/2.0+stlMinY), 0.0);
    CADMesh* flowerpot = new CADMesh("models/flowerpot.stl", "STL", mm, offset, false);
    G4VSolid* flowerpot_solid = flowerpot->TessellatedMesh();
    flowerpot_logical = new G4LogicalVolume(flowerpot_solid, chamber_mat, "flowerpot", 0, 0, 0);
    new G4PVPlacement(0, G4ThreeVector(), flowerpot_logical, "flowerpot", world, false, 0, false);

    // slewing ring
    stlMinX= 0.064685807;
    stlMinY= 0.064685807;
    stlMinZ= 103.3071;
    stlMaxX= 184.93532 ;
    stlMaxY= 184.93532 ;
    stlMaxZ= 137.3071 ;
    offset = G4ThreeVector(-((stlMaxX-stlMinX)/2.0+stlMinX), -((stlMaxY-stlMinY)/2.0+stlMinY), 0);
    CADMesh* slewing_ring = new CADMesh("models/slewing_ring.stl", "STL", mm, offset, false);
    G4VSolid* slewing_ring_solid = slewing_ring->TessellatedMesh();
    slewing_ring_logical = new G4LogicalVolume(slewing_ring_solid, slewing_ring_mat, "slewing_ring", 0,0,0);
    new G4PVPlacement(0, G4ThreeVector(), slewing_ring_logical, "slewing_ring", world, false, 0, false);

    // flange
    stlMinX= 1e-06 ;
    stlMinY= 1e-06 ;
    stlMinZ= 137.3071 ;
    stlMaxX= 121.92 ;
    stlMaxY= 121.92 ;
    stlMaxZ= 218.1606 ;
    offset = G4ThreeVector(-((stlMaxX-stlMinX)/2.0+stlMinX), -((stlMaxY-stlMinY)/2.0+stlMinY), 0);
    CADMesh* flange = new CADMesh("models/flange.stl", "STL", mm, offset, false);
    G4VSolid* flange_solid = flange->TessellatedMesh();
    flange_logical = new G4LogicalVolume(flange_solid, flange_mat, "flange", 0,0,0);
    new G4PVPlacement(0, G4ThreeVector(), flange_logical, "flange", world, false, 0, false);

    // gammasphere shell
    stlMinX= 9.9999999e-07 ;
    stlMinY= 0.71656709 ;
    stlMinZ= 1e-06 ;
    stlMaxX= 1538.4177 ;
    stlMaxY= 1548.6834 ;
    stlMaxZ= 1549.4 ;
    offset = G4ThreeVector(-((stlMaxX-stlMinX)/2.0+stlMinX), 
                           -((stlMaxY-stlMinY)/2.0+stlMinY), 
                           -((stlMaxZ-stlMinZ)/2.0+stlMinZ));
    CADMesh* gammasphere_shell = new CADMesh("models/gammasphere_shell.stl", "STL", mm, offset, false);
    G4VSolid* gammasphere_shell_solid = gammasphere_shell->TessellatedMesh();
    gammasphere_shell_logical = new G4LogicalVolume(gammasphere_shell_solid, gammasphere_shell_mat, "gammasphere_shell", 0, 0, 0);
    new G4PVPlacement(0, G4ThreeVector(), gammasphere_shell_logical, "gammasphere_shell", world, false, 0, false);

    scattering_chamber_logical->SetVisAttributes(invisible);
    flowerpot_logical->         SetVisAttributes(invisible);
    flange_logical->            SetVisAttributes(invisible);
    slewing_ring_logical->      SetVisAttributes(invisible);
    gammasphere_shell_logical->SetVisAttributes(invisible);
}



void Gammasphere::LocateElements(G4LogicalVolume *world)
{

    // parse Gammasphere angle file for detector angles
    G4double GStheta;
    G4double GSphi[5];
    G4double rotPhi[5];
    G4int GSnum[5];
    G4String GStype[5];
    std::ifstream GSfile;
    GSfile.open(GSfilename);
    G4String junk;
    G4ThreeVector position = G4ThreeVector(1., 1., 1.);
    G4RotationMatrix rotation;
    G4Transform3D transform;
    // loop over inputfile
    while (GSfile >> GStheta) 
    {
        GSfile >> GSnum[0] >> GSnum[1] >> GSnum[2]>> GSnum[3] >> GSnum[4];
        GSfile >> GSphi[0] >> GSphi[1] >> GSphi[2]>> GSphi[3] >> GSphi[4];
        GSfile >> GStype[0] >> GStype[1] >> GStype[2]>> GStype[3] >> GStype[4];
        GSfile >> rotPhi[0] >> rotPhi[1] >> rotPhi[2]>> rotPhi[3] >> rotPhi[4];

        getline(GSfile, junk);
        for (int i = 0; i < 5; i++) 
        {
            position.setMag(GS_ge_radius+(ge_length+ge_taper_length)/2.0);
            position.setTheta(GStheta*degree);
            position.setPhi(GSphi[i]*degree);

            rotation=G4RotationMatrix::IDENTITY; 
            rotation.rotateY(GStheta*degree);
            rotation.rotateZ(GSphi[i]*degree);
            transform = G4Transform3D(rotation, position);
            new G4PVPlacement(transform, ge_crystal_logical, "GSCrystal", world, false, GSnum[i], false);

            position.setMag(GS_ge_radius-absorber2_thickness-absorber1_thickness/2.0);
            transform = G4Transform3D(rotation, position);
            new G4PVPlacement(transform, absorber1_logical, "absorber1", world, false, 0, false);
            position.setMag(GS_ge_radius-absorber2_thickness/2.0);
            transform = G4Transform3D(rotation, position);
            new G4PVPlacement(transform, absorber2_logical, "absorber2", world, false, 0, false);

			// bgo backplug
			position.setMag(GS_ge_radius+ge_length+carbon_disk_thickness+backplug_thickness/2.0);
			transform = G4Transform3D(rotation, position);
			new G4PVPlacement(transform, backplug_logical, "backplug", world, false, GSnum[i]+600, false);

			// 
			position.setMag(GS_ge_radius + ge_length + carbon_disk_thickness/2);
			transform = G4Transform3D(rotation, position);
			new G4PVPlacement(transform, carbon_disk_logical, "carbon_disk", world, false, 0, false);

            //position.setMag(GS_BGO_radius+BGO_length/2.0);
            position.setMag(GS_BGO_radius);
            position.setTheta(GStheta*degree);
            position.setPhi(GSphi[i]*degree);

            rotation=G4RotationMatrix::IDENTITY; 
            rotation.rotateY(GStheta*degree);
            rotation.rotateZ(GSphi[i]*degree);
            rotation.setPhi(rotPhi[i]*degree);
            transform = G4Transform3D(rotation, position);

            if (GStype[i][0] == 'B') 
            {
                 new G4PVPlacement(transform, BGOTypeB_alu_logical, "BGOTypeB_alu", world, false, 0, false);
                 new G4PVPlacement(transform, BGOTypeB_logical, "BGOTypeB", world, false, GSnum[i]+400, false);
            } 
            else if (GStype[i][0] == 'C') 
            {
                 new G4PVPlacement(transform, BGOTypeC_logical, "BGOTypeC", world, false, GSnum[i]+400, false);
            } 
            else if (GStype[i][0] == 'D') 
            {
                 new G4PVPlacement(transform, BGOTypeD_logical, "BGOTypeD", world, false, GSnum[i]+400, false);
            } 
            else 
            {
                 G4cerr << "unknown BGO type " << GStype[i] << G4endl;
            }

            position.setMag(GS_BGO_radius-heavimet_thickness);
            transform = G4Transform3D(rotation, position);
            if (GStype[i][0] == 'B') {
            new G4PVPlacement(transform, HM_B_logical, "HM_B", world, false, 0, false);
            } else if (GStype[i][0] == 'C') {
            new G4PVPlacement(transform, HM_C_logical, "HM_C", world, false, 0, false);
           } else if (GStype[i][0] == 'D') {
           new G4PVPlacement(transform, HM_D_logical, "HM_D", world, false, 0, false);
           } else {
             G4cerr << "unknown BGO type " << GStype[i] << G4endl;
          }
      }
  }

}


void Gammasphere::ConstructSDandField()
{
     if (!gammasphereSD) 
     {
        G4cout << "Construction /gammasphereSD sensitive detector" << G4endl;
        gammasphereSD = new DetectorSD("/gammasphereSD", "GammasphereHitsCollection");
        gammasphereSD->SetModuleDeph(0);
    }

     G4SDManager* SDman = G4SDManager::GetSDMpointer();
     SDman->AddNewDetector(gammasphereSD);
     ge_crystal_logical->SetSensitiveDetector(gammasphereSD);
     BGOTypeB_logical->SetSensitiveDetector(gammasphereSD);
     BGOTypeC_logical->SetSensitiveDetector(gammasphereSD);
     BGOTypeD_logical->SetSensitiveDetector(gammasphereSD);
}

//DetectorSD* Gammasphere::gammasphereSD = 0;
