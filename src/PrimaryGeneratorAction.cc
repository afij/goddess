
//
// $Id: PrimaryGeneratorAction.cc 12.10.2016 A Fijalkowska $
//
/// \file PrimaryGeneratorAction.cc
/// \brief Implementation of the PrimaryGeneratorAction class

#include "PrimaryGeneratorAction.hh"

#include "Randomize.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
 : G4VUserPrimaryGeneratorAction(),  particleGun(0)
{
    G4int n_particle = 1;
    particleGun = new G4ParticleGun(n_particle);
	SetUpDefault();

}


PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete particleGun;
}

void PrimaryGeneratorAction::SetUpDefault()
{
	G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
	G4ParticleDefinition* particle = particleTable->FindParticle("gamma");

	particleGun->SetParticleDefinition(particle);
	particleGun->SetParticleTime(0.0*ns);
	particleGun->SetParticlePosition(G4ThreeVector(0.0*cm,0.0*cm,0.0*cm));
	particleGun->SetParticleMomentumDirection(G4ThreeVector(1.,0.,0.));
	particleGun->SetParticleEnergy(500.0*keV);
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{	

    GenerateSingleParticle(anEvent);
}	

void PrimaryGeneratorAction::GenerateSingleParticle(G4Event* anEvent)
{

	G4ThreeVector aim(0.0,1.0,0.0);
	GenerateIsotropicDirectionDistribution(&aim,0.0);
	G4ThreeVector startPos(0.0*cm,0.0*cm,0.0*cm);	
	particleGun->SetParticlePosition(startPos);
	particleGun->SetParticleMomentumDirection(aim);
	particleGun->GeneratePrimaryVertex(anEvent);
}



void PrimaryGeneratorAction::GenerateIsotropicDirectionDistribution
                             (G4ThreeVector* direction,
                             G4double thetaMin, 
                             G4double thetaMax, 
                             G4double phiMin, 
                             G4double phiMax)
{
	G4double cosThetaMin = cos(thetaMin);
	G4double cosThetaMax = cos(thetaMax);
	
	//assumption: cosThetaMin > cosThetaMax 
	G4double randomCosTheta = G4UniformRand()*(cosThetaMin-cosThetaMax) + cosThetaMax;
	G4double randomSinTheta = sqrt(1.0 - randomCosTheta * randomCosTheta);
	G4double randomPhi = G4UniformRand()*(phiMax-phiMin) + phiMin;

	*direction = G4ThreeVector(cos(randomPhi)*randomSinTheta, sin(randomPhi)*randomSinTheta, randomCosTheta);	 
}


void PrimaryGeneratorAction::GenerateIsotropicDirectionDistribution
                             (G4ThreeVector* direction, G4double theta0)
{                                                                            
	// -cos(Theta0) < cosTheta < cos(Theta0)
	G4double cosTheta = ( G4UniformRand() - 0.5 ) * 2.0*cos(theta0);
	G4double sinTheta = sqrt( 1.0 - cosTheta * cosTheta );
	// 0. < phi < 2*pi
	G4double phi = G4UniformRand() * 8.0 * atan(1.0);
	*direction = G4ThreeVector (cos(phi)*sinTheta, sin(phi)*sinTheta, cosTheta);
}



