// $Id: DetectorHit.cc 12.09.2016 A. Fijalkowska $
//
/// \file DetectorHit.cc
/// \brief Implementation of the DetectorHit class
//
//
#include "DetectorHit.hh"
#include "G4ios.hh"
#include "G4VVisManager.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4VPhysicalVolume.hh"
#include "Exception.hh"

G4ThreadLocal G4Allocator<DetectorHit>* DetectorHitsAllocator=0;


DetectorHit::DetectorHit(G4int moduleId, G4double enDep)                                 
{
    moduleIndex = moduleId;
	energyDep = enDep;	
	nrOfInteract = 1;
}                   


DetectorHit::~DetectorHit() {}


DetectorHit::DetectorHit(const DetectorHit &right) : G4VHit()
{ 
   nrOfInteract = right.nrOfInteract;
   moduleIndex = right.moduleIndex;
   energyDep = right.energyDep;
}

    
    
const DetectorHit& DetectorHit::operator=(const DetectorHit &right)
{
   nrOfInteract = right.nrOfInteract;
   moduleIndex = right.moduleIndex;
   energyDep = right.energyDep;
   return *this;
}


G4int DetectorHit::operator==(const DetectorHit &right) const{
  return 0;
}


void DetectorHit::Draw()
{
}


void DetectorHit::Print() {}
	
	
void DetectorHit::AddHit(G4double hitEnergyDep)
{
	energyDep += hitEnergyDep;
	nrOfInteract ++;	
}

