//
// $Id: SteppingAction.cc 12.07.2016 A. Fijalkowska $
//
/// \file SteppingAction.cc
/// \brief Implementation of the SteppingAction class
//
//
#include "SteppingAction.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4StepPoint.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4SystemOfUnits.hh"


SteppingAction::SteppingAction()
{

}

SteppingAction::~SteppingAction() 
{

}



void SteppingAction::UserSteppingAction(const G4Step* theStep)
{

        
    //PrintStep(theStep);  

    
    /*G4Track* theTrack = theStep->GetTrack();
    G4StepPoint* preStepPoint = theStep->GetPreStepPoint();
  
	G4TouchableHandle preStepTouch =preStepPoint->GetTouchableHandle();
	G4VPhysicalVolume* preStepPV = preStepTouch->GetVolume();
	G4LogicalVolume* preStepLV = preStepPV->GetLogicalVolume();
	std::string materialName = preStepLV ->GetMaterial()->GetName(); */
  
  

}


void SteppingAction::PrintStep(const G4Step* theStep)
{
   G4Track* theTrack = theStep->GetTrack();
   G4int trackId = theTrack->GetTrackID();
   G4VPhysicalVolume* volume = theTrack->GetVolume();    
   G4String volumeName = volume->GetName();
   G4double energyDeposit = theStep->GetTotalEnergyDeposit();

    std::cout << " trackID: " << trackId
              << " edep: " << energyDeposit
	          << " voulme name: " << volumeName << std::endl;

}


