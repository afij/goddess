//
// $Id: DetectorSD.cc 12.08.2016 A. Fijalkowska$
//
/// \file DetectorSD.cc
/// \brief Implementation of sensitive detector class
//
//
#include "DetectorSD.hh"
#include "DetectorHit.hh"

#include "G4VPhysicalVolume.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4VTouchable.hh"
#include "G4TouchableHistory.hh"
#include "G4ios.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"

#include "G4SDManager.hh"

DetectorSD::DetectorSD(G4String name, G4String collName)
  : G4VSensitiveDetector(name), moduleDeph(0)
{
  collectionName.insert(collName);
  hitCID = -1;
}


DetectorSD::~DetectorSD() {}


void DetectorSD::Initialize(G4HCofThisEvent* hitsCE)
{ 
  int collNameSize = collectionName.size();
  detectorHitsCollection = new DetectorHitsCollection
                      (SensitiveDetectorName, collectionName[collNameSize-1]);

  if(hitCID<0){
    hitCID = GetCollectionID(collNameSize - 1);
  }
  
  hitsCE->AddHitsCollection( hitCID, detectorHitsCollection);
}


G4bool DetectorSD::ProcessHits(G4Step* aStep, G4TouchableHistory* )
{
//std::cout << "prosecc hit " << collectionName[0] << std::endl;
   G4double energyDep = GetEnergyDeposit(aStep);
   if(energyDep <= 0)
      return false;

  G4int moduleIndex = GetIndex(aStep, moduleDeph);

  G4int hitsNr = detectorHitsCollection->entries();
  G4bool moduleAlreadyHit = false;
  for(G4int i=0; i<hitsNr; i++)
  {
	  G4int hitsModuleIndex = (*detectorHitsCollection)[i]->GetModuleIndex();
      if(moduleIndex == hitsModuleIndex)
      {
         (*detectorHitsCollection)[i]->AddHit(energyDep); 
         moduleAlreadyHit = true;		  
         break;
      }
  }
  
  if(!moduleAlreadyHit)
  {
	  	DetectorHit* aHit = new DetectorHit(moduleIndex, energyDep);
		detectorHitsCollection->insert( aHit );	  
  }

  return true;
}

G4int DetectorSD::GetIndex(const G4Step* aStep, int deph)
{
	G4int voulmeNr =
    aStep->GetPostStepPoint()->GetTouchable()->GetReplicaNumber(deph);
	return voulmeNr;
}

G4double DetectorSD::GetEnergyDeposit(const G4Step* aStep)
{	
	return aStep->GetTotalEnergyDeposit();	
}

G4double DetectorSD::GetHitTime(const G4Step* aStep)
{
    G4StepPoint* postStepPoint = aStep->GetPostStepPoint();
    return postStepPoint->GetGlobalTime();
}

G4ThreeVector DetectorSD::GetPosition(const G4Step* aStep)
{
   G4StepPoint* thePrePoint = aStep->GetPreStepPoint(); 
   G4StepPoint* thePostPoint = aStep->GetPostStepPoint();

   //Get the average position of the hit
   G4ThreeVector pos = thePrePoint->GetPosition() + thePostPoint->GetPosition();
   pos/=2.;
   return pos;	
}
    
void DetectorSD::EndOfEvent(G4HCofThisEvent* ) {}

void DetectorSD::clear() {}

