#include "EventAction.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4VHitsCollection.hh"
#include "G4SDManager.hh"
#include "G4UImanager.hh"
#include "G4ios.hh"

#include "DetectorHit.hh"
#include "AnalysisManager.hh"
extern std::ofstream outFile;

EventAction::EventAction()
{
    orrubaCollID = -1;
    gammasphereCollID = -1;
}

EventAction::~EventAction()
{
}

void EventAction::BeginOfEventAction(const G4Event* evt)
{

}

void EventAction::EndOfEventAction(const G4Event* evt)
{


    G4int eventID = evt->GetEventID();
    if( eventID % 100 == 0 )
    {
        G4cout << "Finished Running Event # " << eventID << G4endl;
    }
    
    G4SDManager* SDman = G4SDManager::GetSDMpointer();
    if(orrubaCollID < 0) 
        orrubaCollID=SDman->GetCollectionID("OrrubaHitsCollection");
    if(gammasphereCollID < 0) 
        gammasphereCollID=SDman->GetCollectionID("GammasphereHitsCollection");
        
        
        
    G4HCofThisEvent* hitsCE = evt->GetHCofThisEvent();
    std::cout << "orrubaID " << orrubaCollID << " gammasphereCollID " << gammasphereCollID << std::endl;
    DetectorHitsCollection* orrubaHC = 0;
    DetectorHitsCollection* gammasphereHC = 0;
    if(hitsCE)
    {
        
        if(orrubaCollID >= 0)
            orrubaHC = (DetectorHitsCollection*)( hitsCE->GetHC(orrubaCollID) );
        if(gammasphereCollID >= 0)
            gammasphereHC = (DetectorHitsCollection*)( hitsCE->GetHC(gammasphereCollID) );
    }
    
    AnalysisManager* analysisManager = AnalysisManager::GetInstance();
  
    if(orrubaHC)
    {
        analysisManager->AddOrrubaHit(orrubaHC, eventID);
    }
    
    if(gammasphereHC)
    {
        std::cout << "event " << orrubaCollID << " " << gammasphereCollID << std::endl;
        analysisManager->AddGammasphereHit(gammasphereHC, eventID);
    }
}  


  
