
// $Id: PrimaryGeneratorAction.hh 30.03.2018 A Fijalkowska $
//
/// \file PrimaryGeneratorAction.hh
/// \brief Definition of the PrimaryGeneratorAction class

#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"

class G4ParticleGun;
class G4Event;
class Decay;

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    PrimaryGeneratorAction();
    virtual ~PrimaryGeneratorAction();

  public:
    virtual void GeneratePrimaries(G4Event*);

  private:
	void SetUpDefault();
	void GenerateIsotropicDirectionDistribution (G4ThreeVector* direction, 
	                                             G4double thetaMin, 
	                                             G4double thetaMax, 
	                                             G4double phiMin, 
	                                             G4double phiMax);
	void GenerateIsotropicDirectionDistribution(G4ThreeVector* direction, 
	                                            G4double theta0);
	void RadiateBarByParallelBeam(G4Event* anEvent, 
	                            G4double barWidth, 
	                            G4double barLength);
	void GenerateSingleParticle(G4Event* anEvent);
    
                                
    G4ParticleGun* particleGun;
};


#endif 

