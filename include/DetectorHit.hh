
// $Id: DetectorHit.hh 12.16.2016 A. Fijalkowska $
//
/// \file DetectorHit.hh
/// \brief Definition of the DetectorHitHit class
//
//
#ifndef DetectorHit_h
#define DetectorHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4LogicalVolume.hh"

class DetectorHit : public G4VHit
{
  public:
 
    DetectorHit(G4int moduleId, G4double enDep);
    virtual ~DetectorHit();
    DetectorHit(const DetectorHit &right);

    const DetectorHit& operator=(const DetectorHit &right);
    G4int operator==(const DetectorHit &right) const;

    inline void *operator new(size_t);
    inline void operator delete(void *aHit);
 
    virtual void Draw();
    virtual void Print();
    
    void AddHit(G4double energyDep);
    inline G4int GetModuleIndex() { return moduleIndex; }
	G4double GetEnergyDeposit() {return energyDep;}
	inline G4int GetNrOfInteractions() {return nrOfInteract;}
	
	
  private:
    G4int nrOfInteract;
    G4int moduleIndex;
    G4double energyDep;

};

typedef G4THitsCollection<DetectorHit> DetectorHitsCollection;

extern G4ThreadLocal G4Allocator<DetectorHit>* DetectorHitsAllocator;

inline void* DetectorHit::operator new(size_t){
  if(!DetectorHitsAllocator)
      DetectorHitsAllocator = new G4Allocator<DetectorHit>;
  return (void *) DetectorHitsAllocator->MallocSingle();
}

inline void DetectorHit::operator delete(void *aHit){
  DetectorHitsAllocator->FreeSingle((DetectorHit*) aHit);
}

#endif

