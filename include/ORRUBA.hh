

#ifndef ORRUBA_H
#define ORRUBA_H 1

#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "DetectorSD.hh"
#include "G4VisAttributes.hh"

class ORRUBA
{
  public:

  ORRUBA();
  ~ORRUBA();

  inline G4int GetNperRing() {return n_sil_per_ring;};

  void Construct(G4LogicalVolume *world);
  void ConstructSDandField();
  private:
  
  void DefineMaterials();
  void DefineColors();
  void ConstructSensitivePart(G4LogicalVolume* world_logical);
  void ConstructNonSensitivePart(G4LogicalVolume* world_logical);

  G4LogicalVolume* sil_E_logical;
  G4LogicalVolume* sil_dE_logical;
  G4LogicalVolume * detector_mounts_logical;
  G4LogicalVolume * ORRUBA_frame_logical;

  // detector parameters
  G4int n_sil_per_ring;
  G4double sil_z_offset;
  G4double sil_length;
  G4double sil_width;
  G4double sil_E_thick;
  G4double sil_dE_thick;
  G4double sil_E_radius;
  G4double sil_dE_radius;
  G4double mainZOffset;


  G4Material *sil_mat;
  G4Material* ORRUBA_frame_mat;
  G4Material* detector_mounts_mat;
  
  G4VisAttributes* yellow;
  G4VisAttributes* gray;
  G4VisAttributes* orange;
  
  DetectorSD* orrubaSD;
};

#endif

