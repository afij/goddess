//
// $Id: DetectorSD.hh 30.01.2018 A. Fijalkowska $
//
/// \file DetectorSD.hh
/// \brief Definition of the Detector sensitive detector class
//
//
#ifndef DetectorSD_h
#define DetectorSD_h 1

#include "G4VSensitiveDetector.hh"
#include "DetectorHit.hh"


class G4Step;
class G4HCofThisEvent;

class DetectorSD : public G4VSensitiveDetector
{

  public:

    DetectorSD(G4String name, G4String collName);
    virtual ~DetectorSD();
 
    virtual void Initialize(G4HCofThisEvent* );
    virtual G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* );
    virtual void EndOfEvent(G4HCofThisEvent* );
    virtual void clear();
    void SetModuleDeph(G4int moduleDephVal) {moduleDeph = moduleDephVal;}
    
  private:

    G4int GetIndex(const G4Step* aStep, int deph);
    G4double GetEnergyDeposit(const G4Step* aStep);
    G4double GetHitTime(const G4Step* aStep);
    G4ThreeVector GetPosition(const G4Step* aStep);
    DetectorHitsCollection* detectorHitsCollection;
    G4int moduleDeph;
    G4int hitCID;

};

#endif
