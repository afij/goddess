#ifndef Gammasphere_H
#define Gammasphere_H 1

#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "DetectorSD.hh"

class Gammasphere
{
    public:

    Gammasphere();
    ~Gammasphere();
    void Construct(G4LogicalVolume *world);
    void ConstructSDandField();
    
    private:
    void DefineMaterials();
    void DefineColors();
    void ConstructGermanium();
    void ConstructBGO();
    void ConstructHMCollimator();
    void ConstructAndLocateAdditionalElements(G4LogicalVolume* world);
    void LocateElements(G4LogicalVolume* world);

    G4LogicalVolume* detector_mounts_logical;
    G4LogicalVolume* scattering_chamber_logical;
    G4LogicalVolume* ge_crystal_logical;
    G4LogicalVolume* BGOTypeB_alu_logical;
    G4LogicalVolume* BGOTypeB_logical;
    G4LogicalVolume* BGOTypeC_logical;
    G4LogicalVolume* BGOTypeD_logical;
    G4LogicalVolume* HM_B_logical;
    G4LogicalVolume* HM_C_logical;
    G4LogicalVolume* HM_D_logical;
    G4LogicalVolume* absorber1_logical;
    G4LogicalVolume* absorber2_logical;
    G4LogicalVolume* flowerpot_logical;
    G4LogicalVolume* slewing_ring_logical;
    G4LogicalVolume* gammasphere_shell_logical;
    G4LogicalVolume* flange_logical;
	G4LogicalVolume* carbon_disk_logical;
	G4LogicalVolume* backplug_logical;

    // detector parameters
    G4double absorber1_thickness;
    G4double absorber2_thickness;

    G4double ge_inner_diameter;
    G4double ge_outer_diameter;
    G4double ge_length;
    G4double ge_taper_diameter;
    G4double ge_taper_length;
    G4double heavimet_thickness;

    G4double GS_ge_radius;
    G4double GS_BGO_radius;
	G4double carbon_disk_diameter;
	G4double carbon_disk_thickness;
	G4double backplug_diameter;
	G4double backplug_thickness;

    G4Material* HM_mat;
    G4Material* BGO_mat;
    G4Material* detector_mounts_mat;
    G4Material* ge_mat;
    G4Material* chamber_mat;
    G4Material* endcaps_mat;
    G4Material* flowerpot_mat;
    G4Material* gammasphere_shell_mat;
    G4Material* slewing_ring_mat;
    G4Material* flange_mat;
    G4Material* absorber2_mat;
    G4Material* absorber1_mat;
    G4Material* carbon_disk_mat;
	G4Material* alu_mat;


    G4VisAttributes* yellow;
    G4VisAttributes* gray;
    G4VisAttributes* white;
    G4VisAttributes* red;
    G4VisAttributes* green;
    G4VisAttributes* blue;
    G4VisAttributes* orange;
    G4VisAttributes* invisible;

    G4String GSfilename;
    DetectorSD* gammasphereSD;
};

#endif

