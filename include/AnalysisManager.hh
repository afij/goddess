// $Id: AnalysisManager.hh 30.03.2018 A. Fijalkowska $
//


#ifndef AnalysisManager_h
#define AnalysisManager_h 1

#include "G4RootAnalysisManager.hh"
#include "G4String.hh"
#include "DetectorHit.hh"

class AnalysisManager
{
	public:

		void CreateOutput(G4String filename);
		void SaveOutput();
		void AddNrOfEvent(G4int nrOfEvents);
		void AddOrrubaHit(DetectorHitsCollection* orrHitsColl, G4int eventId);
		void AddGammasphereHit(DetectorHitsCollection* gammHitsColl, G4int eventId);
		
	private:
		AnalysisManager();
		virtual ~AnalysisManager() {}
		void CreateTuple();
		void CreateOrrubaTuple();
		void CreateGammasphereTuple();
		void CreateNrOfCountsTuple();
		
		
		static AnalysisManager *s_instance;
		G4RootAnalysisManager* rootManager;
		G4int nrOfCreatedTuple;		
		G4int orrubaTupleId;
		G4int gammasphereTupleId;
		G4int nrOrCountsTupleId;
		
	public:
	static AnalysisManager* GetInstance()
	{
		if (!s_instance)
			s_instance = new AnalysisManager();
		return s_instance;
	}
		
};

#endif
